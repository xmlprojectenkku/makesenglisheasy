<?php

/* @var $this yii\web\View */
header("content-type:text/xml;charset=UTF-8");
use yii\helpers\Html;

$this->title = 'NEWS';
$this->params['breadcrumbs'][] = $this->title;

?>
<h2>Bangkok Post</h2>
<div class="row">
     <div class="col s6">
<?php

$rss = simplexml_load_file('http://www.bangkokpost.com/rss/data/most-recent.xml');
echo '<h4 style="color: grey">Most recent</h4>';
echo '<div class="collection">';
foreach ($rss->channel->item as $item) {
    echo '<li>' . '<a href="' . $item->link . '" class="collection-item">' . $item->title . "</a></li>";
}
echo "</div>";
?>
</div>
<div>

<div class="col s6">
<?php

$rss = simplexml_load_file('http://www.bangkokpost.com/rss/data/topstories.xml');
echo '<h4 style="color: grey">Top stories</h4>';
echo '<div class="collection">';
foreach ($rss->channel->item as $item) {
    echo '<li>' . '<a href="' . $item->link . '" class="collection-item">' . $item->title . "</a></li>";
}
echo "</div>";
?>
</div>
</div>
</div>


