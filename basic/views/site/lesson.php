<?php

use yii\helpers\Html;

$this->title = 'Lesson';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class='Lesson'>
    <div class="container">
        <div class="page-header">
            <h1>Practice with video <small><span class="glyphicon glyphicon-expand" aria-hidden="true"></span></small></h1>
        </div>
        <div class="row">
            <div class="col-md-6">
                <p class="lean">Learn English with Emma [engVid]</p>
                <div class="video-container">
                    <iframe width="550" height="350" src="https://www.youtube.com/embed/2gcfHpGpsOw" frameborder="0" allowfullscreen></iframe>
                </div>
            </div>
            <div class="col-md-6">
                <p class="lean">JamesESL English Lessons (engVid)</p>
                <div class="video-container">
                    <iframe width="550" height="350" src="https://www.youtube.com/embed/GEQhDeNyM8s" frameborder="0" allowfullscreen></iframe>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="page-header">
            <h1>Improve reading skill with Bangkok post article <small><span class="glyphicon glyphicon-expand" aria-hidden="true"></span></small></h1>
            <?php
            $rss = simplexml_load_file('http://www.bangkokpost.com/rss/data/topstories.xml');
            $i = 0;
            foreach ($rss->channel->item as $item) {
                if ($i < 3) {
                    echo '<p class="flow-text">' . $item->title . '</p>';
                    echo '<li>Description<p>' . $item->description . '</p></li>';
                }
                $i++;
            }
            echo' <a class="btn btn-default" href="index.php?r=site/bangkokpost" role="button">Read more</a>';
            ?>
        </div>
    </div>
    <div class="container">
        <h1>Exercise</h1>
        <div class="row">
            <div class="col-md-6">
                <div class="panel panel-success">
                    <div class="panel-heading">
                        <h3 class="panel-title">Exercise 1</h3>
                    </div>
                    <div class="panel-body">
                        Panel content
                    </div>
                </div>    
            </div>
            <div class="col-md-6">
                <div class="panel panel-success">
                    <div class="panel-heading">
                        <h3 class="panel-title">Exercise 2</h3>
                    </div>
                    <div class="panel-body">
                        Panel content
                    </div>
                </div>    
            </div>
        </div>
    </div>
</div>
