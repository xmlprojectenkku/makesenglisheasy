<?php

use yii\helpers\Html;

$this->title = 'Video';
$before = 'Lesson';
$this->params['breadcrumbs'][] = $this->title;

//$rss = simplexml_load_file("http://rss.cnn.com/rss/edition_travel.rss");
?>
<div class='youtube'>
    <h2>Youtube Channel </h2>
    <div class="container-fluid">
        <div class="row">
            <div class="col s6">
                <h5>English room</h5>
                <iframe width="450" height="315" src="https://www.youtube.com/embed/msIGoYzQ1Ys" frameborder="0" allowfullscreen></iframe>
                 <a class="waves-effect waves-light btn" href="#" role="button">More</a> 
            </div>
            <div class="col s6">
                <h5>Engvid</h5>
                <iframe width="450" height="315" src="https://www.youtube.com/embed/-W_rB9xWfYk" frameborder="0" allowfullscreen></iframe>
                <br>
                <a class="waves-effect waves-light btn" href="#" role="button">More</a>
            </div>
        </div>
    </div>
</div>

<div class='cnn'>
    <h2>CNN NEWS</h2>
    <div class="container-fluid">
        <div class="row">
            <div class="col s6">
                <h6>How beer mega merger will impact Africa's breweries</h6>
                <iframe width='450' height='315' src='http://edition.cnn.com/video/api/embed.html#/video/world/2015/11/09/spc-marketplace-africa-beer-market-south-africa-a.cnn' frameborder='0'></iframe>
                <a class="waves-effect waves-light btn" href="#" role="button">More</a>
            </div>
            <div class="col s6">
                <h6>Indonesia fires intentionally set to grow palm oil</h6>
                <iframe width='450' height='315' src='http://edition.cnn.com/video/api/embed.html#/video/world/2015/11/09/indonesia-palm-oil-molko-pkg.cnn' frameborder='0'></iframe>
                <a class="waves-effect waves-light btn" href="#" role="button">More</a>
            </div>
        </div>
    </div>
</div>