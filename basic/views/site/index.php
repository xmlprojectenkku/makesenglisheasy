
<?php

use yii\helpers\Html;
use app\assets\FontAsset;

FontAsset::register($this);
/* @var $this yii\web\View */
$this->title = 'Makes English easy';
?>

<style>
    .body_edited{
        background-color: rgb(24, 188, 156);
    }
</style>

<body>
    <section class="body_edited">
        <br/><br/><br/><br/><br/><br/><br/><br/><br/>
        <center>
            <img src="https://cdn3.iconfinder.com/data/icons/gray-toolbar-3/512/earth-512.png" height="200" width="200" class="img-responsive" alt="Responsive image">
            <br/>
            <font size="10" color="white">Let's us open your new world<br/></font>
            <font size="6" color="white">A big English class room on internet<br/></font>
            <br/><br/>
            <br/><br/><br/><br/><br/>
        </center>
    </section>

    <section class="#">
        <h1>Words of the day</h1>
        <ul>
            <?php
            $rss = simplexml_load_file('http://www.thefreedictionary.com/_/WoD/rss.aspx') or die("Error: Cannot create object");
            foreach ($rss->channel->item as $item) {
                echo "<li><a href=" . $item->link . ">" . $item->title . "</a></li>";
                echo "<p>" . $item->description . "</p>";
            }
            ?>
        </ul>
    </section>
    <!--Spelling Bee by TheFreeDictionary.com-->
    <div style="width:350px;position:relative;background-color:;padding:4px">
        <div style="font:bold 12pt '';color:#000000">Spelling Bee</div>
        <style>
            #TfdBee {border:1px #000000 solid;background-color:;padding:2px}
            #TfdBee {font:normal 10pt '';color:#000000}
            #TfdBee A {color:#0000FF}
            #TfdBee .spell_word {font-size:110%;clear:all;margin-bottom:5px}
            #TfdBee INPUT { font-size: 11pt; margin:0; padding:0;}
            #tfd_bee_sound {float:left;width:40px;height:40px;margin-right:5px}
            #tfd_bee_sound IMG {width:100%;height:100%;border:none}
            #tfd_bee_def {font-size:80%}
            #tfd_bee_score {float:right;margin:0 0 5px 2px}
            .tfd_bee_correct {background-color: lightgreen}
            .tfd_bee_wrong {background-color: pink}
            .tfd_bee_na {background-color: white}
        </style>
        <div id="TfdBee">
            <form name="tfd_bee_f" style="display:inline;margin:0">
                <div id="tfd_bee_difficulty">
                    difficulty&nbsp;level: <span style="white-space:nowrap">
                        <input type="radio" name="level" value="1" id="tfd_bee_level1" 
                               onclick="tfd_level_click(this)"/><label for="tfd_bee_level1">easy</label>
                        <input type="radio" name="level" value="2" id="tfd_bee_level2" 
                               onclick="tfd_level_click(this)" checked="yes"/><label for="tfd_bee_level2">hard</label>
                        <input type="radio" name="level" value="3" id="tfd_bee_level3" 
                               onclick="tfd_level_click(this)"/><label for="tfd_bee_level3">expert</label></span>
                </div><div id="tfd_bee_score">score: -</div>
                <div id="tfd_bee_sound"></div>
                <div id="tfd_bee_def"><b>please wait...</b></div>
                <div id="tfd_bee_answ" style="font-size:110%">&nbsp;</div>
                <div class="spell_word">spell the word:
                    <input type="text" class="tfd_bee_na" id="tfd_bee_uword" size="12"/></div>
                <input type="button" onclick="tfd_bee_answer()" value="answer"/>
                <input type="button" onclick="tfd_bee_new()" value="new word"/>
                <script type="text/javascript" src="http://img.tfd.com/daily/spellbee-top.js" charset="UTF-8"></script>
            </form></div><div style="font:normal 8pt '';color:#000000">
            <a style="color:#000000" href="http://www.thefreedictionary.com/lookup.htm#Spelling-Bee" rel="nofollow">Spelling Bee</a>
            provided by <a style="color:#000000" href="http://www.thefreedictionary.com/">TheFreeDictionary.com</a>
        </div></div>
    <!--end of Spelling Bee-->
    <br/>
    <!--Match Up by TheFreeDictionary.com-->
    <div style="width:350px;position:relative;background-color:;padding:4px">
        <div style="font:bold 12pt '';color:#000000">Match Up</div>
        <style>
            #MatchUp {width:100%;border:1px #000000 solid;background-color:}
            #MatchUp TD {font:normal 10pt '';color:#000000}
            #MatchUp A {color:#0000FF}
            #tfd_MatchUp INPUT.tfd_txt {border:1px black solid;height:16pt;font-size:10pt;width:100px;cursor:pointer;margin-top:2px;margin-right:4px;text-align:center}
        </style>
        <form name="SynMatch" method="get" action="http://www.thefreedictionary.com/_/MatchUp.aspx" style="display:inline;margin:0" target="_top">
            <table id="MatchUp">
                <tr><td>
                        <script type="text/javascript"
                        src="http://img.tfd.com/daily/matchup.js" charset="UTF-8"></script>
                        Match each word in the left column with its synonym on the right. When finished, click Answer to see the results. Good luck!<br/><br/><center>
                    <input type="button" value="Clear" onclick="tfd_mw_clear()"/>&nbsp;<input type="submit" value="Answer" onclick="this.form.res.value = tfd_mw_answers"/></center>
                </td></tr></table></form>
        <div style="font:normal 8pt '';color:#000000">
            <a style="color:#000000" href="http://www.thefreedictionary.com/lookup.htm#Match-Up" rel="nofollow">Match Up</a>
            provided by <a style="color:#000000" href="http://www.thefreedictionary.com/">TheFreeDictionary.com</a>
        </div></div>
    <!--end of Match Up-->
    <section>

    </section>
</body>
